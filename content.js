// ---------------------------------------------------------------------------------------------------------------------
//
//      Beatport Spotify Search
//      Chrome Extension
//      Version 1.1
//      Author: Pavel "Pao" Antolík
//      Check out http://beatsbypao.com/en/ for more!
//
// ---------------------------------------------------------------------------------------------------------------------

let ctrlDown = false;

document.addEventListener('keydown', event => {
  if (event.ctrlKey) {
    ctrlDown = true;
	document.body.style.cursor = 'pointer';
	document.body.classList.add("ctrl");
  }
});

document.addEventListener('keyup', event => {
  if (event.key === 'Control') {
    ctrlDown = false;
	document.body.style.cursor = 'default';
	document.body.classList.remove("ctrl");
  }
});

const getArtistNames = function(domElement) {

	let artistsString = '';

	$(domElement).find('a').each(function(){
		artistsString += $(this).text().trim() + " ";
	});

	return artistsString.substring(0, artistsString.length - 1);
}

const spotifySearch = (e) => {
		let releaseTitle = $(e.target).find('.cell.title > .container > a:first-child').text().trim();
		let releaseArtists = getArtistNames($(e.target).find('.cell.title > .container > div'));
		let searchTerm = releaseArtists + " " + releaseTitle;
		if(ctrlDown){
			window.open('https://open.spotify.com/search/' + encodeURIComponent(searchTerm), '_blank');
		}
}

// ---------- RUN THAT SHIT --------------------------------------------------------------------------------------------

$(document).ready(function(){

	let contentLoadedInterval = setInterval(bindRows, 100);

	function bindRows() {
		const table = document.querySelector('main div[role="table"]');
		if (table) {
			clearInterval(contentLoadedInterval);
			table.querySelectorAll(':scope > div:last-child > .row').forEach((row) => {
				console.log(row);
				row.addEventListener('click', spotifySearch);
			});
		}
	}
})
