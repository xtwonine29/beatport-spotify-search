# BEATPORT SPOTIFY SEARCH

Just a little Chrome extension that adds a bit of special functionality to Beatport.

CTRL + CLICK on a release to open pre-filled Spotify search.

Notes: Doesn't work with AJAX pagination, you have to refresh the page each time.
